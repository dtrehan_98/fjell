﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPauseListener
{
    bool IsPaused { get; set; }
}
