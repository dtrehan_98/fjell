﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EngineValueSelection
{
    public string selectionName;
    public IndexStringProperty category;
    public IndexStringProperty engineValue;
    public EngineValueData valueData;
}
