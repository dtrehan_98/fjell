﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UILayoutEngineValue
{
    public UILayoutMaster layoutMaster;
    public UIEngineValue[] engineValues;
}

