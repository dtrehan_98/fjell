﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class NGNStateVariableHandler
    {
        [SerializeField] protected string variableName;
        [SerializeField] protected VariableType variableType;
    }
}


