﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace NGN
{
    [CustomEditor(typeof(NGNEntityStateController))]
    public class NGNStateControllerEditor : Editor
    {
        protected SerializedObject sourceRef;
        protected NGNEntityStateController source;
        protected SerializedProperty variables;
        protected SerializedProperty states;
        protected SerializedProperty transitions;

        protected System.Type[] variableTypes;
        protected string[] variableTypeNames;

        protected System.Type[] transitionTypes;
        protected string[] transitionTypeNames;

        protected GUIStyle buttonStyle;

        protected virtual void OnEnable()
        {
            source = (NGNEntityStateController)target;
            sourceRef = serializedObject;
            GetTypes();
            GetProperties();
        }

        
        public override void OnInspectorGUI()
        {
            SetProperties();
            sourceRef.ApplyModifiedProperties();
            Repaint();
        }

        protected virtual void GetProperties()
        {
            variables = sourceRef.FindProperty("variables");
            states = sourceRef.FindProperty("states");
            transitions = sourceRef.FindProperty("transitions");

        }

        protected virtual void SetProperties()
        {
            GetStyles();
            DisplayFoldout("Variables", variables, DisplayVariables);
            DisplayFoldout("States", states, DisplayState);
            DisplayFoldout("Transitions", transitions, DisplayTransitions);
        }

        private void GetTypes()
        {
            variableTypes = typeof(NGNValue).GetAllSubclasses();
            variableTypeNames = typeof(NGNValue).GetAllSubclassesNames();
            transitionTypes = typeof(NGNTransition).GetAllSubclasses();
            transitionTypeNames = typeof(NGNTransition).GetAllSubclassesNames();
        }

        private void GetStyles()
        {
            if (buttonStyle == null)
                buttonStyle = new GUIStyle(EditorStyles.miniButton);
        }


        private void DisplayFoldout(string _name,SerializedProperty _arrayProperty, System.Action<SerializedProperty, int> _elementField)
        {
            EditorGUILayout.BeginHorizontal();

            _arrayProperty.isExpanded = EditorGUILayout.Foldout(_arrayProperty.isExpanded, "");
            GUILayout.Space(-50);
            EditorExtensions.LabelFieldCustom(_name, FontStyle.Bold, Color.black, 11, 60);
            GUILayout.FlexibleSpace();
            if (_arrayProperty.isExpanded)
            {
                if (GUILayout.Button("Add " + _name, GUILayout.Width(80)))
                    _arrayProperty.InsertArrayElementAtIndex(_arrayProperty.LastIndex());
            }
            EditorGUILayout.EndHorizontal();

            if (!_arrayProperty.isExpanded)
                return;

            for (int i = 0; i < _arrayProperty.arraySize; i++)
            {
                var ele = _arrayProperty.GetArrayElementAtIndex(i);
                _elementField.Invoke(ele, i);
            }
            
        }

        #region VARIABLES

        private void DisplayVariables(SerializedProperty _property, int _ind)
        {
            EditorGUI.indentLevel++;
            ValueSelectionField(_property, _ind);
            EditorGUI.indentLevel--;
        }

        private void ValueSelectionField(SerializedProperty _property, int _ind)
        {
            
            if (_property.objectReferenceValue == null)
            {
                var val = CreateInstance<NGNValue>();
                _property.objectReferenceValue = val;

            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                //get properties
                var ser = new SerializedObject(_property.objectReferenceValue);
                var expanded = ser.FindProperty("expanded");
                var variableName = ser.FindProperty("propertyName");
                var typeInd = ser.FindProperty("typeInd");

                //foldout
                expanded.boolValue = EditorGUILayout.Foldout(expanded.boolValue,"");

                //name field
                GUILayout.Space(-70);
                variableName.stringValue = EditorGUILayout.TextField(variableName.stringValue);
                //type field
                GUILayout.Space(-15);
                typeInd.intValue = EditorGUILayout.Popup(typeInd.intValue, variableTypeNames, GUILayout.Width(100));
                //switch obj if different desired value
                var desType = variableTypes[typeInd.intValue];
                var curType = _property.objectReferenceValue.GetType();
                if (desType != curType)
                {
                    var obj = CreateInstance(desType);
                    _property.objectReferenceValue = obj;
                    var obSer = new SerializedObject(_property.objectReferenceValue);
                    var varName = obSer.FindProperty("propertyName");
                    var tInd = obSer.FindProperty("typeInd");
                    varName.stringValue = variableName.stringValue;
                    tInd.intValue = typeInd.intValue;
                    obSer.ApplyModifiedProperties();
                    obSer.Dispose();
                    return;
                }
                //delete button
                if (GUILayout.Button("X", buttonStyle, GUILayout.Width(20)))
                {
                    _property.objectReferenceValue = null;
                    variables.DeleteArrayElementAtIndex(_ind);
                    return;
                }
                    
                EditorGUILayout.EndHorizontal();
                if (expanded.boolValue)
                {
                    var it = ser.GetIterator();
                    while (it.NextVisible(true))
                    {
                        if (it.name == "m_Script") continue;
                        EditorGUILayout.PropertyField(it);
                    }
                }
                //prop
                //EditorGUILayout.PropertyField(_property);
                ser.ApplyModifiedProperties();
                ser.Dispose();
            }
            
        }


        #endregion

        #region STATES

        private void DisplayState(SerializedProperty _property, int _ind)
        {
            //props
            var stateName = _property.FindPropertyRelative("stateName");
            var priority = _property.FindPropertyRelative("priority");
            var continuous = _property.FindPropertyRelative("continuous");
            var finishedCondition = _property.FindPropertyRelative("finishedCondition");
            var stateCallMask = _property.FindPropertyRelative("stateCallMask");
            //actions
            var onEnterActions = _property.FindPropertyRelative("onEnterActions");
            var onUpdateActions = _property.FindPropertyRelative("onUpdateActions");
            var onFixedUpdateActions = _property.FindPropertyRelative("onFixedUpdateActions");
            var onLateUpdateActions = _property.FindPropertyRelative("onLateUpdateActions");
            var onFinishedActions = _property.FindPropertyRelative("onFinishedActions");

            _property.isExpanded = EditorGUILayout.Foldout(_property.isExpanded, stateName.stringValue);

            if (!_property.isExpanded)
                return;

            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.BeginHorizontal();
            if (stateName.stringValue == "")
                stateName.stringValue = "State " + _ind;
            stateName.stringValue = EditorGUILayout.TextField(stateName.stringValue);
            if (GUILayout.Button("X", buttonStyle, GUILayout.Width(20)))
            {
                states.DeleteArrayElementAtIndex(_ind);
                return;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(priority);
            EditorGUILayout.PropertyField(continuous);
            if (!continuous.boolValue)
                EditorGUILayout.PropertyField(finishedCondition);
            stateCallMask.intValue = EditorGUILayout.MaskField("State Mask", stateCallMask.intValue, System.Enum.GetNames(typeof(NGNStateHandler.StateCallType)));

            if (stateCallMask.intValue == (stateCallMask.intValue | (1 << (int)NGNStateHandler.StateCallType.OnEnter)))
                DisplayStateType(onEnterActions);
            if (stateCallMask.intValue == (stateCallMask.intValue | (1 << (int)NGNStateHandler.StateCallType.OnUpdate)))
                DisplayStateType(onUpdateActions);
            if (stateCallMask.intValue == (stateCallMask.intValue | (1 << (int)NGNStateHandler.StateCallType.OnFixedUpdate)))
                DisplayStateType(onFixedUpdateActions);
            if (stateCallMask.intValue == (stateCallMask.intValue | (1 << (int)NGNStateHandler.StateCallType.OnLateUpdate)))
                DisplayStateType(onLateUpdateActions);
            if (stateCallMask.intValue == (stateCallMask.intValue | (1 << (int)NGNStateHandler.StateCallType.OnFinished)))
                DisplayStateType(onFinishedActions);

            EditorGUILayout.EndVertical();

        }

        private void DisplayStateType(SerializedProperty _property)
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(_property.displayName);
            if (GUILayout.Button("New Stack"))
            {
                _property.InsertArrayElementAtIndex(_property.LastIndex());
                var ele = _property.GetArrayElementAtIndex(0);
                var obj = CreateInstance<NGNActionStack>();
                ele.objectReferenceValue = obj;

            }
            if (GUILayout.Button("Load Stack"))
            {
                Debug.Log("Adding later");

            }

            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel++;
            for (int i = 0; i < _property.arraySize; i++)
            {
                var ele = _property.GetArrayElementAtIndex(i);
                DisplayActionStack(ele, _property, i);
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.EndVertical();
        }

        private void DisplayActionStack(SerializedProperty _property, SerializedProperty _array, int _ind)
        {
           
            EditorGUILayout.BeginVertical("Box");
            GUILayout.Space(6);
            var ser = new SerializedObject(_property.objectReferenceValue);
            var stackName = ser.FindProperty("stackName");
            var actions = ser.FindProperty("actions");
            EditorGUILayout.BeginHorizontal();
            if (stackName.stringValue == "")
                stackName.stringValue = "Stack";
            stackName.stringValue = EditorGUILayout.TextField(stackName.stringValue);
            if (GUILayout.Button("Add Action"))
                NGNActionSearchEditor.ShowWindow(_property.objectReferenceValue, sourceRef.targetObject);
            if (GUILayout.Button("Save"))
            {
                Debug.Log("Adding later");
            }
            if (GUILayout.Button("X"))
            {
                _property.objectReferenceValue = null;
                _array.DeleteArrayElementAtIndex(_ind);
                return;
            }

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3);
            for (int i = 0; i < actions.arraySize; i++)
            {
                var ele = actions.GetArrayElementAtIndex(i);
                DisplayActionField(ele, i);
            }
            ser.Dispose();
            EditorGUILayout.EndVertical();

        }

        private void DisplayActionField(SerializedProperty _property, int _ind)
        {
            EditorGUILayout.PropertyField(_property);

            if (_property.objectReferenceValue)
            {
                //set gameobject owner of action
                var ser = new SerializedObject(_property.objectReferenceValue);
                var owner = ser.FindProperty("owner");
                if (owner.objectReferenceValue == null)
                {
                    owner.objectReferenceValue = source.gameObject;
                    ser.ApplyModifiedProperties();
                }
                ser.Dispose();
            }
   
            
        }

        #endregion

        #region TRANSITIONS

        private void DisplayTransitions(SerializedProperty _property, int _ind)
        {
            var anyState = _property.FindPropertyRelative("anyState");
            var prevState = _property.FindPropertyRelative("prevState");
            var transition = _property.FindPropertyRelative("transition");
            var nextState = _property.FindPropertyRelative("nextState");

            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.PropertyField(anyState);
            if (!anyState.boolValue)
                prevState.IndexStringField(source.GetStateNames());
            TransitionField(transition, _ind);
            nextState.IndexStringField(source.GetStateNames());
            EditorGUILayout.EndVertical();
        }

        private void TransitionField(SerializedProperty _property, int _ind)
        {
            if (transitionTypeNames.Length < 1)
                return;

            if (_property.objectReferenceValue == null)
            {
                var val = CreateInstance<NGNTransition>();
                _property.objectReferenceValue = val;

            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                //get properties
                var ser = new SerializedObject(_property.objectReferenceValue);
                var expanded = ser.FindProperty("expanded");
                var variableName = ser.FindProperty("propertyName");
                var typeInd = ser.FindProperty("typeInd");

                //foldout
                var style = new GUIStyle(EditorStyles.foldout);
                style.fixedWidth = 10;
                expanded.boolValue = EditorGUILayout.Foldout(expanded.boolValue, "", style);

                //type field
                GUILayout.Space(-50);
                typeInd.intValue = EditorGUILayout.Popup(typeInd.intValue, transitionTypeNames);
                
                //switch obj if different desired value
                var desType = transitionTypes[typeInd.intValue];
                var curType = _property.objectReferenceValue.GetType();
                if (desType != curType)
                {
                    var obj = CreateInstance(desType);
                    _property.objectReferenceValue = obj;
                    var obSer = new SerializedObject(_property.objectReferenceValue);
                    var varName = obSer.FindProperty("propertyName");
                    var tInd = obSer.FindProperty("typeInd");
                    varName.stringValue = variableName.stringValue;
                    tInd.intValue = typeInd.intValue;
                    obSer.ApplyModifiedProperties();
                    obSer.Dispose();
                    return;
                }
                //delete button
                if (GUILayout.Button("X", buttonStyle, GUILayout.Width(20)))
                {
                    _property.objectReferenceValue = null;
                    transitions.DeleteArrayElementAtIndex(_ind);
                    return;
                }

                EditorGUILayout.EndHorizontal();
                if (expanded.boolValue)
                {
                    EditorGUILayout.PropertyField(_property);
                }
                //prop
                //EditorGUILayout.PropertyField(_property);
                ser.ApplyModifiedProperties();
                ser.Dispose();
            }
        }

        #endregion
    }
}


