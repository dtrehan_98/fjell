﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Reflection;

namespace NGN
{
    public class NGNActionSearchEditor : EditorWindow
    {
        [System.Serializable]
        public class ActionSearchData
        {
            public Object component;
            public NGNActionStack actionStackObj;
        }

        
        private static ActionSearchData data = new ActionSearchData();
        private static SerializedProperty dataSer;
        protected string input;
        protected System.Type[] subclasses;
        protected System.Type[] matches = new System.Type[0];
        static protected List<Object> objCopy = new List<Object>();
        protected const string dataPath = "Assets/NGN/Resources/OdinData/ActionSearchData";

        public static void ShowWindow(object _target, Object _component)
        {
            CarryOverData(_target, _component);
            GetWindow<NGNActionSearchEditor>("Action Search");
        }

        static void CarryOverData(object _target, Object _component)
        {
            data.actionStackObj = _target as NGNActionStack;
            data.component = _component;
        }

        private void OnEnable()
        {
            subclasses = typeof(NGNAction).GetAllSubclasses();
        }

        private void OnGUI()
        {
            //focus on text on boot
            GUI.SetNextControlName("text");
            if (matches.Length == 1)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Debug.Log("yay");
                    AssignAction(matches[0]);
                }
                    
            }
            //search bar
            var style = new GUIStyle(EditorStyles.toolbarSearchField);
            
            input = EditorGUILayout.TextField("Search", input, style);
            EditorGUI.FocusTextInControl("text");

            if (input != null && input != "")
                input = Regex.Replace(input, @"\s+", ""); //remove spaces in input 
            //get matches
            matches = FindMatches(subclasses, input);

            //display matches
            for (int i = 0; i < matches.Length; i++)
            {
                if (GUILayout.Button(matches[i].Name))
                {
                    AssignAction(matches[i]);
                }
                    
            }

        }

        private void AssignAction(System.Type _type)
        {
            var obj = ScriptableObject.CreateInstance(_type);
            obj.name = _type.Name;
            var ser = new SerializedObject(data.actionStackObj);
            var actions = ser.FindProperty("actions");
            actions.InsertArrayElementAtIndex(actions.LastIndex());
            var ele = actions.GetArrayElementAtIndex(actions.LastIndex());
            ele.objectReferenceValue = obj;
            ser.ApplyModifiedProperties();
            
            Selection.activeObject = data.component;
            ser.Dispose();
        }

        private static System.Type[] FindMatches(System.Type[] _types, string _input)
        {
            var matches = new List<System.Type>();
            if (_input != null && _input != "")
            {
                for (int i = 0; i < _types.Length; i++)
                {
                    CultureInfo info = CultureInfo.CurrentUICulture;
                    var name = _types[i].Name;
                    if (info.CompareInfo.IndexOf(name, _input, CompareOptions.OrdinalIgnoreCase) >= 0)
                    {
                        matches.Add(_types[i]);
                    }
                }
            }
            return matches.ToArray();
        }

    }

}

