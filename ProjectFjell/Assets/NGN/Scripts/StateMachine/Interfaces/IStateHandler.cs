﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public interface IStateHandler
    {
        bool IsActive { get; set; }
    }
}


