﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class NGNFloat : NGNValue, IValueNumeric<float>
    {
        [SerializeField] protected float floatValue;
        [SerializeField] protected float floatMin;
        [SerializeField] protected float floatMax;
        public float Value { get { return floatValue; } set { floatValue = value; } }
        public float MinValue { get { return floatMin; } set { floatMin = value; } }
        public float MaxValue { get { return floatMax; } set { floatMax = value; } }
    }
}


