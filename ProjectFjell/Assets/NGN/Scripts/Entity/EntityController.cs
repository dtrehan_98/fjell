﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public abstract class EntityController : MonoBehaviour
    {
        protected virtual void FixedUpdate()
        {
            DoLocomotion();
        }

        protected abstract void DoLocomotion();
    }
}


