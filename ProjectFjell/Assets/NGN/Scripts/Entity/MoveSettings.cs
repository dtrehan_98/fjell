﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class MoveSettings
    {
        [SerializeField] private float speed;
        public float Speed { get { return speed; } }
    }
}


