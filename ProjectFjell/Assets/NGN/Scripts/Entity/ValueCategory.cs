﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class ValueCategory
    {
        [SerializeField] private string categoryName;
        public string CategoryName { get { return categoryName; } }
        [SerializeField] private int typeInd;
        public int TypeInd { get { return typeInd; } }
        [SerializeField] private NGNValue[] values;
    }
}


