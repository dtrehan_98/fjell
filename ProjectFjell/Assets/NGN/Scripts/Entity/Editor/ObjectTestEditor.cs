﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NGN
{
    [CustomEditor(typeof(ObjectTest))]
    public class ObjectTestEditor : Editor
    {
        protected SerializedObject sourceRef;
        protected ObjectTest source;

        protected SerializedProperty objects;

        protected virtual void OnEnable()
        {
            source = (ObjectTest)target;
            sourceRef = serializedObject;
            GetProperties();
        }

        public override void OnInspectorGUI()
        {
            SetProperties();
            sourceRef.ApplyModifiedProperties();
        }

        protected virtual void GetProperties()
        {
            objects = sourceRef.FindProperty("objects");
        }

        protected virtual void SetProperties()
        {
            DisplayEntityData();
        }

        protected virtual void DisplayEntityData()
        {
            EditorExtensions.LabelFieldCustom("Value Manager", FontStyle.Bold);
            objects.ArrayFieldButtons("Test", true, true, true, true, Object);
        }

        void Object(SerializedProperty _prop, int _index)
        {
            EditorGUILayout.PropertyField(_prop);
        }
    }
}


