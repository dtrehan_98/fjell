﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class UserController : EntityController
    {
        [SerializeField] protected MoveSettings moveSettings;
        [SerializeField] protected InputAxisReader horizontalInput;
        [SerializeField] protected InputAxisReader verticalInput;
        [SerializeField] protected RotateSettings rotateSettings;
        [SerializeField] protected InputAxisReader rotationInput;

        private ITick move;
        private ITick rotate;

        protected virtual void Awake()
        {
            move = new Move(transform, verticalInput, horizontalInput, moveSettings);
            rotate = new Rotate(transform, rotationInput, rotateSettings);
        }

        protected override void DoLocomotion()
        {
            move.Tick();
            rotate.Tick();
        }
    }
}


