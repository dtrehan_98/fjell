﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class Move : ITick
    {
        readonly Transform transform;
        readonly IReadInput verticalInput;
        readonly IReadInput horizontalInput;
        readonly MoveSettings moveSettings;

        public Move(Transform _transform, IReadInput _verticalInput, IReadInput _horizontalInput, MoveSettings _moveSettings)
        {
            transform = _transform;
            verticalInput = _verticalInput;
            horizontalInput = _horizontalInput;
            moveSettings = _moveSettings;
        }

        public void Tick()
        {
            verticalInput.ReadInput();
            horizontalInput.ReadInput();
            var move = new Vector3(horizontalInput.InputPower, 0, verticalInput.InputPower).normalized;
            transform.Translate(move * moveSettings.Speed * Time.deltaTime);
        }
    }
}


