﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public interface IValue<T>
    {
        T Value { get; }
    }
}

