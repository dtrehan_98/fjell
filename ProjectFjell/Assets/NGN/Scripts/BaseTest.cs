﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseTest : ScriptableObject
{
    public virtual void DoIt() { }
}
