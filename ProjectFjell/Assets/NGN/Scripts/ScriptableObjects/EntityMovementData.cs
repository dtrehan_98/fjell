﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [CreateAssetMenu(menuName = "NGN/Data/Entity/MovementData", order = 1)]
    public class EntityMovementData : NGNScriptableObject
    {
        [SerializeField] private float movementSpeed = 5;
        public float MovementSpeed { get { return movementSpeed; } }
    }
}


