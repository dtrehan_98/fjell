﻿
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Globalization;

namespace NGN
{
    [CustomPropertyDrawer(typeof(NGNTransition), true)]
    public class NGNTransitionPropertyDrawer : NGNScriptableObjectPropertyDrawer
    {
        protected Type[] types;
        protected string[] typesNames;

        protected override void GetProperties(SerializedObject ser)
        {
            if (!initialized)
            {
                types = typeof(NGNTransition).GetAllSubclasses();
                typesNames = typeof(NGNTransition).GetAllSubclassesNames();
            }
            base.GetProperties(ser);   
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            DisplayTransition(position, property, label);
            property.serializedObject.ApplyModifiedProperties();
            EditorGUI.EndProperty();
        }

        protected virtual void DisplayTransition(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.objectReferenceValue == null)
            {
                var val = ScriptableObject.CreateInstance<NGNTransition>();
                property.objectReferenceValue = val;
            }
            else
            {
                var foldoutRect = new Rect(position.x, position.y, 30, EditorGUIUtility.singleLineHeight);
                expanded.boolValue = EditorGUI.Foldout(foldoutRect, expanded.boolValue, "");

                var typeRect = new Rect(position.x + 30, position.y, position.width - 30, EditorGUIUtility.singleLineHeight);
                //typeInd.intValue = EditorGUI.Popup(typerec);
            }
        }


    }
}


