// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System.Linq;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Logic)]
	[Tooltip("Sends Events based on the comparison of a Float against many Floats.")]
	public class FloatCompareAny : FsmStateAction
	{
		[RequiredField]
        [Tooltip("The second float variable.")]
		public FsmFloat[] floatValues;

		[RequiredField]
        [Tooltip("Tolerance for the Equal test (almost equal).\nNOTE: Floats that look the same are often not exactly the same, so you often need to use a small tolerance.")]
		public FsmFloat tolerance;

		[Tooltip("Event sent if Float 1 equals Float 2 (within Tolerance)")]
		public FsmEvent anyEqual;

        [Tooltip("Event sent if Float 1 is less than Float 2")]
		public FsmEvent anyLessThan;
		
        [Tooltip("Event sent if Float 1 is greater than Float 2")]
		public FsmEvent anyGreaterThan;
		
        [Tooltip("Repeat every frame. Useful if the variables are changing and you're waiting for a particular result.")]
        public bool everyFrame;

		public override void Reset()
		{
			floatValues = null;
			tolerance = 0f;
			anyEqual = null;
			anyLessThan = null;
			anyGreaterThan = null;
			everyFrame = false;
		}

		public override void OnEnter()
		{
			DoCompare();
			
			if (!everyFrame)
			{
			    Finish();
			}
		}

		public override void OnUpdate()
		{
			DoCompare();
		}

		void DoCompare()
		{

			if (floatValues.Any(x => x.Value == tolerance.Value))
			{
				Fsm.Event(anyEqual);
				return;
			}

			if (floatValues.Any(x => x.Value < tolerance.Value))
			{
				Fsm.Event(anyLessThan);
				return;
			}

			if (floatValues.Any(x => x.Value > tolerance.Value))
			{
				Fsm.Event(anyGreaterThan);
			}

		}

		public override string ErrorCheck()
		{
			if (FsmEvent.IsNullOrEmpty(anyEqual) &&
				FsmEvent.IsNullOrEmpty(anyLessThan) &&
				FsmEvent.IsNullOrEmpty(anyGreaterThan))
				return "Action sends no events!";
			return "";
		}

        
#if UNITY_EDITOR
	    public override string AutoName()
	    {
	        return ActionHelpers.AutoName(this, floatValues);
	    }
#endif
	}
}