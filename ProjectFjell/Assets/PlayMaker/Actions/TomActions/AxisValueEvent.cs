// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Input)]
	[Tooltip("Sends events based on value of input Axis")]
	public class AxisValueEvent : FsmStateAction
	{
		public enum CompareType { Greater, Less, Equal, NotEqual}

		[Tooltip("Horizontal axis as defined in the Input Manager")]
		public FsmString axis;

		[Tooltip("Horizontal axis as defined in the Input Manager")]
		public FsmFloat compareValue;

		[Tooltip("What comparison triggers the event?")]
		public CompareType compareType;

		[Tooltip("Event to send if input is in any direction.")]
		public FsmEvent sendEvent;

		private float _axisValue;

		public override void Reset()
		{
			axis = null;
			compareValue = null;
			compareType = CompareType.Greater;
			sendEvent = null;
		}

		public override void OnUpdate()
		{	
			_axisValue = axis.Value != "" ? Input.GetAxis(axis.Value) : 0;

			if (compareType == CompareType.Greater && _axisValue > compareValue.Value ||
				compareType == CompareType.Less && _axisValue < compareValue.Value ||
				compareType == CompareType.Equal && _axisValue == compareValue.Value ||
				compareType == CompareType.NotEqual && _axisValue != compareValue.Value)
				Fsm.Event(sendEvent);
		}
	}
}

