// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Physics)]
    [Tooltip("Gets the velocity trajectory points of a rigidbody and stores it in an FSMArray")]
    public class GetTrajectoryPredictionPoints : FsmStateAction
    {
        public enum UpdateType { OnUpdate, OnFixedUpdate, OnLateUpdate }

        [Tooltip("The start position of the object")]
        public FsmOwnerDefault startPosition;

        [Tooltip("The start position of the object")]
        public FsmVector3 startPositionVector;

        [Tooltip("The object we are predicting")]
        [RequiredField]
        [CheckForComponent(typeof(Rigidbody))]
        public FsmGameObject rigidbodyObject;

        [Tooltip("The velocity direction of the object")]
        public FsmVector3 velocity;

        [Tooltip("The velocity force of the object")]
        public FsmFloat velocityMultiplier;

        [Tooltip("The amount position points in the array")]
        public FsmInt positionCount;

        [Tooltip("The amount of time between each position count. " +
            "Increase this while lowering Position Count for better performance.")]
        public FsmFloat timeStepMultiplier;

        [Tooltip("Store the position points")]
        [UIHint(UIHint.Variable), RequiredField, Readonly]
        [ArrayEditor(VariableType.Vector3)]
        public FsmArray storePositions = null;

        [Tooltip("Detect the predicted collision")]
        public bool detectCollision;

        [Tooltip("Only include these layers")]
        public int collisionMask;

        [Tooltip("Stop gettings points if there is a collision")]
        public bool clampPositionArrayToCollision;

        [Tooltip("Store the collided object")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmGameObject collisionObject;

        [Tooltip("Store the collision point")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmVector3 collisionPoint;

        [Tooltip("Store the collision normal")]
        [UIHint(UIHint.Variable), Readonly]
        public FsmVector3 collisionNormal;

        [Tooltip("Repeat every frame.")]
        public bool everyFrame;

        [Tooltip("Update order options for storing data")]
        public UpdateType updateType;

        private Ray ray;
        private RaycastHit rayHit;
        private GameObject go;
        private Rigidbody rb;
        private Vector3 startPos;
        public Vector3[] PosArray { get; private set; }
        public List<Vector3> PosList { get; private set; }

        public override void Reset()
        {
            rigidbodyObject = null;
            velocity = null;
            velocityMultiplier = 1f;
            positionCount = 2;
            timeStepMultiplier = 0.01f;
            storePositions = null;
            detectCollision = false;
            collisionMask = -1;
            clampPositionArrayToCollision = false;
            collisionObject = null;
            collisionPoint = null;
            collisionNormal = null;
            updateType = UpdateType.OnUpdate;
            everyFrame = false;
        }

        public override void OnPreprocess()
        {
            if (updateType == UpdateType.OnFixedUpdate) Fsm.HandleFixedUpdate = true;
            if (updateType == UpdateType.OnLateUpdate) Fsm.HandleLateUpdate = true;
        }

        public override void OnEnter()
        {
            if (everyFrame) return;
            DoPrediction();
            Finish();
        }

        public override void OnUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnUpdate)
                DoPrediction();
        }

        public override void OnFixedUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnFixedUpdate)
                DoPrediction();
        }

        public override void OnLateUpdate()
        {
            if (everyFrame && updateType == UpdateType.OnLateUpdate)
                DoPrediction();
        }

        public void DoPrediction()
        {
            go = Fsm.GetOwnerDefaultTarget(startPosition);
            startPos = go ? go.transform.position : startPositionVector.Value;
            rb = rigidbodyObject.Value.GetComponent<Rigidbody>();

            if (!rb) return;

            //clamp position count
            positionCount.Value = Mathf.Clamp(positionCount.Value, 2, int.MaxValue);

            //clamp time step
            timeStepMultiplier.Value = Mathf.Clamp(timeStepMultiplier.Value, 0.01f, Mathf.Infinity);

            //do prediciton math
            PosArray = GetPhysicsPredictionPoints(startPos, velocity.Value * velocityMultiplier.Value,
                rb.drag, positionCount.Value, timeStepMultiplier.Value);

            //store only values needed in a list in case of collision options
            PosList = new List<Vector3>();
            for (int i = 0; i < PosArray.Length; i++)
            {
                if (detectCollision && i != 0)
                {
                    var dir = PosArray[i] - PosArray[i - 1];
                    ray = new Ray(PosArray[i - 1], dir.normalized);
                    if (Physics.Raycast(ray, out rayHit, dir.magnitude, collisionMask))
                    {
                        collisionObject.Value = rayHit.collider.gameObject;
                        collisionPoint.Value = rayHit.point;
                        collisionNormal.Value = rayHit.normal;
                        break;
                    }
                }
                PosList.Add(PosArray[i]);
            }

            //store list positions in FSM array
            storePositions.Resize(PosList.Count);
            for (int i = 0; i < PosList.Count; i++)
                storePositions.Values[i] = PosList[i];

        }

        Vector3[] GetPhysicsPredictionPoints(Vector3 _startPos, Vector3 _velocity, float _drag, int _steps, float _time)
        {
            var pos = _startPos;
            var vel = _velocity;
            var acc = Physics.gravity;
            var dt = Time.fixedDeltaTime / Physics.defaultSolverVelocityIterations;
            var drag = 1 - (_drag * dt);
            var points = new Vector3[_steps];
            points[0] = _startPos;
            for (int i = 1; i < _steps; i++)
            {
                float t = 0;
                while (t < _time)
                {
                    vel += acc * dt;
                    pos += vel * dt;
                    pos *= drag;
                    t += dt;
                }
                points[i] = pos;
            }
            return points;
        }
    }
}

