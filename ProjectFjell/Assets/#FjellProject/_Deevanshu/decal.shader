﻿Shader "BlendedDecal"
{
	Properties
	{
		_Color("Tint", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
	    _EmissionColor("Color", Color) = (0.000000,0.000000,0.000000,1.000000)
        _EmissionMap("Emission", 2D) = "white" { }
	}

		SubShader
	{
		Lighting on
		ZTest LEqual
		ZWrite Off
		Tags {"Queue" = "Transparent"}
		Pass
		{
			Alphatest Greater 0
			Blend SrcAlpha OneMinusSrcAlpha
			Offset -1, -1
			SetTexture[_MainTex]
			{
				ConstantColor[_Color]
				Combine texture * constant
			}
		}
	}
}
