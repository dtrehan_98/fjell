
Shader "Dev/WaterWaves" {
    Properties {
        _Color ("Color", Color) = (0.2432958,0.3601896,0.5514706,1)
        _Noise ("Noise", 2D) = "white" {}
        _Centerglow ("Center glow", Float ) = 3
        _TransmissionandLightWrapping ("Transmission and Light Wrapping", Float ) = 1.2
        _Uspeedwaves ("U speed waves", Float ) = -0.0253
        _Vspeedwaves ("V speed waves", Float ) = -0.2
        _Uspeed2waves ("U speed2 waves", Float ) = 0.1
        _Vspeed2waves ("V speed2 waves", Float ) = -0.3
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Uspeedwaves;
            uniform float _Vspeedwaves;
            uniform float _Uspeed2waves;
            uniform float _Vspeed2waves;
            uniform float _Centerglow;
            uniform float _TransmissionandLightWrapping;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 node_2617 = _Time;
                float2 node_357 = ((float2(_Uspeedwaves,_Vspeedwaves)*node_2617.g)+i.uv0);
                float4 node_7343 = tex2D(_Noise,TRANSFORM_TEX(node_357, _Noise));
                float2 node_9535 = ((float2(_Uspeed2waves,_Vspeed2waves)*node_2617.g)+i.uv0);
                float4 node_1325 = tex2D(_Noise,TRANSFORM_TEX(node_9535, _Noise));
                float3 diffuseColor = (_Color.rgb+saturate((saturate(((node_7343.r*node_1325.g)*2.0+-1.0))*(pow((1.0 - i.uv0.g),1.4)*_Centerglow))));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Uspeedwaves;
            uniform float _Vspeedwaves;
            uniform float _Uspeed2waves;
            uniform float _Vspeed2waves;
            uniform float _Centerglow;
            uniform float _TransmissionandLightWrapping;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float4 node_2617 = _Time;
                float2 node_357 = ((float2(_Uspeedwaves,_Vspeedwaves)*node_2617.g)+i.uv0);
                float4 node_7343 = tex2D(_Noise,TRANSFORM_TEX(node_357, _Noise));
                float2 node_9535 = ((float2(_Uspeed2waves,_Vspeed2waves)*node_2617.g)+i.uv0);
                float4 node_1325 = tex2D(_Noise,TRANSFORM_TEX(node_9535, _Noise));
                float3 diffuseColor = (_Color.rgb+saturate((saturate(((node_7343.r*node_1325.g)*2.0+-1.0))*(pow((1.0 - i.uv0.g),1.4)*_Centerglow))));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
