
Shader "Dev/Waterfall" {
    Properties {
        _Color ("Color", Color) = (0.3152573,0.5275735,0.875,1)
        _Usppedwaves ("U spped waves", Float ) = 0
        _Vspeedwaves ("V speed waves", Float ) = 0.5
        _WaterTex ("WaterTex", 2D) = "white" {}
        _WaveNoise ("WaveNoise", 2D) = "white" {}
        [MaterialToggle] _Usegradient ("Use gradient?", Float ) = 1
        _Vertexpower ("Vertex power", Float ) = 1
        _TransmissionandLightWrapping ("Transmission and Light Wrapping", Float ) = 1.2
        [MaterialToggle] _Splashvertex ("Splash vertex?", Float ) = 0
        [MaterialToggle] _Splash ("Splash?", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float _Usppedwaves;
            uniform float _Vspeedwaves;
            uniform sampler2D _WaterTex; uniform float4 _WaterTex_ST;
            uniform sampler2D _WaveNoise; uniform float4 _WaveNoise_ST;
            uniform fixed _Usegradient;
            uniform float _Vertexpower;
            uniform float _TransmissionandLightWrapping;
            uniform fixed _Splashvertex;
            uniform fixed _Splash;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7356 = _Time;
                float2 node_9907 = ((float2(_Usppedwaves,_Vspeedwaves)*node_7356.g)+o.uv0);
                float4 _WaveNoise_var = tex2Dlod(_WaveNoise,float4(TRANSFORM_TEX(node_9907, _WaveNoise),0.0,0));
                float node_7244 = (lerp( 1.0, saturate((1.0 - o.uv0.g)), _Usegradient )*_WaveNoise_var.r);
                v.vertex.xyz += (lerp( node_7244, (_WaveNoise_var.r*o.uv0.b), _Splashvertex )*v.normal*_Vertexpower);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 node_7356 = _Time;
                float2 node_9907 = ((float2(_Usppedwaves,_Vspeedwaves)*node_7356.g)+i.uv0);
                float4 _WaterTex_var = tex2D(_WaterTex,TRANSFORM_TEX(node_9907, _WaterTex));
                float4 _WaveNoise_var = tex2D(_WaveNoise,TRANSFORM_TEX(node_9907, _WaveNoise));
                float node_7244 = (lerp( 1.0, saturate((1.0 - i.uv0.g)), _Usegradient )*_WaveNoise_var.r);
                float3 diffuseColor = lerp( saturate(((_Color.rgb*_WaterTex_var.r)+saturate((node_7244*15.0+-5.0)))), _Color.rgb, _Splash );
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float _Usppedwaves;
            uniform float _Vspeedwaves;
            uniform sampler2D _WaterTex; uniform float4 _WaterTex_ST;
            uniform sampler2D _WaveNoise; uniform float4 _WaveNoise_ST;
            uniform fixed _Usegradient;
            uniform float _Vertexpower;
            uniform float _TransmissionandLightWrapping;
            uniform fixed _Splashvertex;
            uniform fixed _Splash;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7356 = _Time;
                float2 node_9907 = ((float2(_Usppedwaves,_Vspeedwaves)*node_7356.g)+o.uv0);
                float4 _WaveNoise_var = tex2Dlod(_WaveNoise,float4(TRANSFORM_TEX(node_9907, _WaveNoise),0.0,0));
                float node_7244 = (lerp( 1.0, saturate((1.0 - o.uv0.g)), _Usegradient )*_WaveNoise_var.r);
                v.vertex.xyz += (lerp( node_7244, (_WaveNoise_var.r*o.uv0.b), _Splashvertex )*v.normal*_Vertexpower);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping)*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(_TransmissionandLightWrapping,_TransmissionandLightWrapping,_TransmissionandLightWrapping);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float4 node_7356 = _Time;
                float2 node_9907 = ((float2(_Usppedwaves,_Vspeedwaves)*node_7356.g)+i.uv0);
                float4 _WaterTex_var = tex2D(_WaterTex,TRANSFORM_TEX(node_9907, _WaterTex));
                float4 _WaveNoise_var = tex2D(_WaveNoise,TRANSFORM_TEX(node_9907, _WaveNoise));
                float node_7244 = (lerp( 1.0, saturate((1.0 - i.uv0.g)), _Usegradient )*_WaveNoise_var.r);
                float3 diffuseColor = lerp( saturate(((_Color.rgb*_WaterTex_var.r)+saturate((node_7244*15.0+-5.0)))), _Color.rgb, _Splash );
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Usppedwaves;
            uniform float _Vspeedwaves;
            uniform sampler2D _WaveNoise; uniform float4 _WaveNoise_ST;
            uniform fixed _Usegradient;
            uniform float _Vertexpower;
            uniform fixed _Splashvertex;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7356 = _Time;
                float2 node_9907 = ((float2(_Usppedwaves,_Vspeedwaves)*node_7356.g)+o.uv0);
                float4 _WaveNoise_var = tex2Dlod(_WaveNoise,float4(TRANSFORM_TEX(node_9907, _WaveNoise),0.0,0));
                float node_7244 = (lerp( 1.0, saturate((1.0 - o.uv0.g)), _Usegradient )*_WaveNoise_var.r);
                v.vertex.xyz += (lerp( node_7244, (_WaveNoise_var.r*o.uv0.b), _Splashvertex )*v.normal*_Vertexpower);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
